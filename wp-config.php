<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'atletas');

/** MySQL database username */
define('DB_USER', 'atletas');

/** MySQL database password */
define('DB_PASSWORD', '*1Nt3nt4l0ats');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w+S>}J(tC66Yzao?ng[eb0$Ta~9%/}x|DKol:6QsmMY;.C~#n-5<8mg;/_tzt0vS');
define('SECURE_AUTH_KEY',  'bK:D}|tVKF^#+-wnVT!7hEg3uQ=M-x=Y-X*q$J-`MeqKh0C+y$|b#97-C3HMJ)lU');
define('LOGGED_IN_KEY',    '1L!8ou0r/S+Wv]>-2O@A5Mm+b$frz(Ed|%x,NZ1M$s;|>-~&4W+6>:bCV^;8`cED');
define('NONCE_KEY',        '!^2@FL[lfa$<wRu2-3fF_iy;|n5P;*Vek`xemJ[)}#mj|1;n3rG+`ro%[)Aj7Lx1');
define('AUTH_SALT',        ' h?u~O/s|#-$c;Q6lEl,|O3!E9n%)o5`s|hH%%s)d]SRUJ*hQ!{;*-MbSunesVom');
define('SECURE_AUTH_SALT', 'q(YFB>Q`g$WB4R5FOB|$wsrC9vXxg[s3QxNY`/u&B;_|zRy+<Gg+y5AVUkbi.7Og');
define('LOGGED_IN_SALT',   'Tue+)pt1s<ZM-:US%H*+V,z}fRIzDk9e|#o4yZ>ai+K CBanOX#n4dLB`RO.r<6F');
define('NONCE_SALT',       '|)|3KNzo0&y&34heH.IqB]nfo{q[B~3+8wI.y)KzE-TqY,E3i9*y+pvo1r&)YRNj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define ('WPLANG', 'es_MX');
