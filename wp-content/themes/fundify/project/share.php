<?php
/**
 * Campaign sharing.
 *
 * @package Fundify
 * @since Fundify 1.5
 */

global $post;

if (class_exists('ID_Project')) {
	$idsocial_settings = maybe_unserialize(get_option('idsocial_settings'));
	if (!empty($idsocial_settings)) {
		$app_id = $idsocial_settings['app_id'];
		$social_settings = $idsocial_settings['social_checks'];
		$settings = (object) $social_settings;
	}
}

$message = apply_filters( 'fundify_share_message', sprintf( __( 'Check out %s on %s! %s', 'fundify' ), $post->post_title, get_bloginfo( 'name' ), get_permalink() ) );
?>

<div class="entry-share">
	<?php _e( 'Share this campaign', 'fundify' ); ?>
	<?php if (isset($settings->prod_page_twitter) && $settings->prod_page_twitter) { ?>
	<a href="<?php echo esc_url( sprintf( 'http://twitter.com/home?status=%s', urlencode( $message ) ) ); ?>" target="_blank" class="share-twitter"><i class="icon-twitter"></i></a>
	<?php } ?>
	<?php if (isset($settings->prod_page_google) && $settings->prod_page_google) { ?>
	<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" class="share-google"><i class="icon-gplus"></i></a>
	<?php } ?>
	<?php if (isset($settings->prod_page_fb) && $settings->prod_page_fb) { ?>
	<?php $image = ID_Project::get_project_thumbnail($post->post_id); ?>
	<a href="https://www.facebook.com/dialog/share?app_id=<?php echo $app_id; ?>&display=popup&href=<?php echo urlencode(get_permalink()); ?>&redirect_uri=<?php echo urlencode(get_permalink()); ?>" class="share-facebook" target="_blank"><i class="icon-facebook"></i></a>
	<?php } ?>
	<?php if (isset($settings->prod_page_pinterest) && $settings->prod_page_pinterest) { ?>
	<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
	<div class="pinit-button">
	<a href="http://pinterest.com/pin/create/button/?url=&media=<?php echo urlencode($image); ?>" data-pin-config="above" target="_blank" class="share-pinterest"></a>
	</div>
	<?php } ?>
	<a href="<?php the_permalink(); ?>" target="_blank" class="share-link"><i class="icon-link"></i></a>
	
	<a href="share-widget" class="share-widget fancybox"><i class="icon-code"></i></a>

	<div id="share-widget" class="fundify-modal">
		<?php get_template_part( 'modal', 'project-widget' ); ?>
	</div>
</div>