<?php
/**
 * Campaign title.
 *
 * @package Fundify
 * @since Fundify 1.5
 */

global $post;
$company_name = get_post_meta($post->ID, 'ign_company_name', true);
?>

<div class="title <?php echo (empty($company_name) ? '' : 'title-two'); ?> pattern-<?php echo rand(1,4); ?>">
	<div class="container">
		<h1><?php the_title() ;?></h1>
		
		<?php if ( !empty($company_name) ) : ?>
		<h3><?php printf( __( 'By %s', 'fundify' ), $company_name ); ?></h3>
		<?php endif; ?>
	</div>
</div>