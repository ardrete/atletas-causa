<div class="md-requiredlogin login">
	<h3><?php _e('Ingresa a tu cuenta para armar un proyecto', 'memberdeck'); ?>.</h3>
    <p><a  href="<?php echo site_url(); ?>/register"><?php _e('Si aún no eres parte de ACC regístrate', 'memberdeck'); ?>.</a></p>

	<?php if (isset($_GET['login_failure']) && $_GET['login_failure'] == 1) {
		echo '<p class="error">Las datos son invalidos</p>';
	} ?>
	<?php if (!is_user_logged_in()) { ?>
		<?php
		$durl = md_get_durl();
		$args = array('redirect' => 'dashboard/?create_project=1',
			'echo' => false);
		echo wp_login_form($args); ?>
	<?php } 
	do_action('idc_below_login_form');
	?>
	<p><a class="lostpassword" href="<?php echo site_url(); ?>/lostpassword/"><?php _e('Recuperar contraseña', 'memberdeck'); ?></a></p>
</div>
