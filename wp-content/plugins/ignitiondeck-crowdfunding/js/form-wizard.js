var FormWizard = function ($) {


    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    var handleDatePickers= function(){
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    };
    
    var moveForm = function(){
      	var form = $("#fes").detach();
        $(form).insertBefore("#main");

        $("#main").css("display", "none");

    };

    var passValuesToResume = function(event){
        event = event || window.event;
        var $target = $(event.target);
        $("#fundacion-resumen").text($("#fundaciones option:selected").text());

        $("#meta-resumen").text(formatNumber($("#fondos").val()));

        $("#actividad-resumen").text($("#deporte").val());

        $("#distancia-resumen").text($("#distancia").val());

        $("#valor-km-resumen").text(formatNumber($("#valorMts").text()));

        $("#evento-resumen").text($("#competencia").val());


        var dateEnds = +$('#fechaFinaliza').parent().datepicker("getDate");
        var today = +(new Date());

        var miliseconds = dateEnds - today;

        if (typeof miliseconds === "number" && !isNaN(miliseconds)) {
            var one_day = 1000 * 60 * 60 * 24;
            var one_hour = 1000 * 60 * 60;
            var one_minute = 1000 * 60;

            var days = miliseconds / one_day;

            miliseconds = days % 1;

            var hours = miliseconds / one_hour;

            miliseconds = hours % 1;

            var minutes = miliseconds / one_minute;

            $("#dias-resumen").text(parseInt(days));
            $("#hrs-resumen").text(parseInt(hours));
            $("#min-resumen").text(parseInt(minutes));
        }
        event.stopPropagation();


    }

    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    var changeCalculationKm = function (event) {
        event = event || window.event;
        var $target = $(event.target);

        var itemSelected = $target.attr("id");
        var distancia, fondosRecaudar;;

        switch (itemSelected) {
            case "fondos":
                distancia = parseFloat($("#distancia").val());
                fondosRecaudar = parseFloat($target.val());
                break;
            case "distancia":
                distancia = parseFloat($target.val());
                fondosRecaudar = parseFloat($("#fondos").val());
                break;
        }
        distancia = distancia || 0;
        fondosRecaudar = fondosRecaudar || 0;

        $("#distanciaKm").text(distancia);
        var distanceMeters = distancia * 1000;
        var distanciaString =   formatNumber(distanceMeters);
        $("#distanciaMts").text(distanciaString);
        var fondosString = formatNumber(fondosRecaudar);
        $("#fondosRecaudar").text("$ " + fondosString);


        var valorMts = fondosRecaudar/distanceMeters || 0;
        var valorMtsString = formatNumber(valorMts.toFixed(2));
        $("#valorMts").text("$ "+ valorMtsString);
        $("#hidValorMts").val(valorMts);

        event.stopPropagation();
    };

    var handleCantidades = function(event){
        event = event || window.event;
        var $target = $(event.target);
        var data = $target.data();
        $("#fondos").val(data.value);
        $("#fondos").trigger("change");

    };

    var handleDeportes = function(event){
        event = event || window.event;
        var $target = $(event.target);
        $target = $target.is("span") ? $target : $target.closest("span");
        var data = $target.data();

        $("#deporte").val(data.value);
    };

    function render(filereader, target, canvas, maxheight){
        var src = filereader.result;
        var image = new Image();
        image.onload = function(){
            if(image.height > maxheight) {
                image.width *= maxheight / image.height;
                image.height = maxheight;
            }
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.width = image.width;
            canvas.height = image.height;
            ctx.drawImage(image, 0, 0, image.width, image.height);
            $(canvas).css("display", "block");
            $(target).find("div").css("display", "none");
        };
        image.src = src;

    }

    var loadImage = function (src, target, canvas, canvasResumen){
        //	Prevent any non-image file type from being read.
        if(!src.type.match(/image.*/)){
            console.log("The file is not an image: ", src.type);
            return;
        }

        //	Create our FileReader and run the results through the render function.
        var reader = new FileReader();
        reader.onload = function(e){
            render(e.target, target, canvas, 150);
            if(canvasResumen){
                render(e.target, undefined, canvasResumen, 500);
            }
        };
        reader.readAsDataURL(src);
    }

    var subscribeDragAndDrop = function(idCover, idCanvas, idCanvasResumen){
        var target = document.getElementById(idCover);
        var canvas = document.getElementById(idCanvas);
        var canvasResumen = document.getElementById(idCanvasResumen);
        target.addEventListener("dragover", function(e){e.preventDefault();}, true);
        target.addEventListener("drop", function(e){
            e.preventDefault();
            loadImage(e.dataTransfer.files[0], target, canvas, canvasResumen);
        }, true);

    };


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }



            $("#hdnPhotoFile").on("change", function(event){
                event = event || window.event;
                var file = event.target.files[0];
                var target = document.getElementById("photoCover");
                var canvas = document.getElementById("photoCanvas");
                loadImage(file, target, canvas);

            });

            $("#hdnProjectFile").on("change", function(event){
                event = event || window.event;
                var file = event.target.files[0];
                var target = document.getElementById("projectCover");
                var canvas = document.getElementById("projectCanvas");
                var canvasResumen = document.getElementById("photoResumen");
                loadImage(file, target, canvas, canvasResumen);
            });

            $("#photoCover div").click(function(e){
                $('#hdnPhotoFile').click();
            });

            $("#projectCover div").click(function(e){
                $("#hdnProjectFile").click();

            });


            subscribeDragAndDrop("photoCover", "photoCanvas");
            subscribeDragAndDrop("projectCover", "projectCanvas", "photoResumen");

            handleDatePickers();
            $('#photoProfile').on('fileselect',function(event, numFiles, label) {
                loadImage();
            });

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' />&nbsp;&nbsp;" + state.text;
            }

            $("#estados").select2({
                placeholder: "Selecciona",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var currentState = $("#currentState").val().trim();

            $("#estados").select2("val", currentState);


            $("#fundaciones").select2({
                placeholder: "Selecciona",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });


            var form = $('#fes');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //Perfil
                    usuario: {
                        maxlength: 30,
                        required: true
                    },
                    nombre:{
                        required: true
                    },
                    apellidos: {
                        maxlength: 30,
                        required: true
                    },
                    correo: {
                        required: true,
                        email: true
                    },
                    telefono:{
                        required: true,
                        maxlength: 10,
                        number: true
                    },
                    estado:{
                        required: true
                    },
                    deporte:{
                        required: true
                    },
                    razon:{
                        required: true,
                        maxlength: 100
                    },
                    //Proyecto
                    fundacion: {
                        required: true
                    },
                    competencia:{
                       required: true
                    },
                    distancia:{
                        required: true,
                        number: true,
                        maxlength: 4
                    },
                    fechaFinaliza:{
                        required: true,
                        date: true
                    },
                    cantidad:{
                        required: true,
                        number: true
                    },
                    descripcion:{
                        required: true
                    }

                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.validator.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]:checked').each(function(){
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                Metronic.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    /*
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);
                    */
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function (event) {
                event = event || window.event;
                event.preventDefault();
                var $target = $(event.target);

                if($target.data("isSubmitting")){
                    return;
                }
                $target.attr("disabled", "disabled");
                $target.data("isSubmitting", true);


                var formData= new FormData();

                var fundacion = $("#fundaciones option:selected").text();
                fundacion = fundacion.trim();
                formData.append("company_name", fundacion);
                formData.append("user_firstname", $("#nombre").val()); //New
                formData.append("user_lastname", $("#apellidos").val());  //New
                formData.append("user_email", $("#correoElectronico").val());  //New
                formData.append("user_phone", $("#telefono").val()); // New
                formData.append("user_state", $("#estados").select2("val")); //New
                formData.append("project_activity", $("#deporte").val()); //New
                formData.append("project_reason", $("#razon").val());
                formData.append("project_name", $("#usuario").val());
                formData.append("project_event", $("#competencia").val()); //New
                formData.append("project_distance", $("#distancia").val()); //New
                formData.append("project_end", $("#fechaFinaliza").val());
                formData.append("project_goal", $("#fondos").val());
                formData.append("project_long_description", $("#descripcion").val())
                formData.append("project_end_type", "open");
                formData.append("project_hero_removed", "no");
                formData.append("project_image2_removed", "no");
                formData.append("project_image3_removed", "no");
                formData.append("project_image4_removed", "no");
                formData.append("project_levels", "1");
                formData.append("project_category", fundacion);
                formData.append("project_level_title[]", "Valor Por Defecto");
                formData.append("project_level_price[]", $("#hidValorMts").val());
                formData.append("project_level_limit[]", 9999);
                formData.append("project_fund_type[]", "capture");
                formData.append("level_description[]", "");
                formData.append("project_fesubmit", "Submit for Review");

                var now = new Date();
                formData.append("project_start", (now.getMonth()+1)+"/"+now.getDate()+"/" + now.getFullYear());



                //Images
                canvasProfilePicture = document.getElementById("photoCanvas");
                canvasProfilePicture.toBlob(function(blob){
                    formData.append("company_logo", blob, "company_logo.jpeg");
                    canvasProjectPicture = document.getElementById("photoResumen");
                    canvasProjectPicture.toBlob(function(blob2){
                        formData.append("project_hero", blob2, "project_wallpaper.jpeg");
                        var options = {};
                        options.url = window.location.href;
                        options.type = "POST";
                        options.data = formData;
                        options.contentType = false;
                        options.processData = false;
                        options.success = function(result) {
                            $("#modal-success").modal("show");
                        };
                        options.error = function(err) {
                            alert(err.statusText);
                            $target.removeAttr("disabled");
                            $target.data("isSubmitting", false);
                        };

                        $.ajax(options);

                    }, "image/*");



                }, "image/*");






            }).hide();

            $("#cantidades span").click(handleCantidades);

            $("#deportes span").click(handleDeportes);

            $("#fondos, #distancia").on("change", changeCalculationKm);

            $("#fundaciones, #fondos, #fechaFinaliza, #competencia, #valorKm, #distancia, #deporte").change(passValuesToResume);
            
            moveForm();
        }

    };

}(jQuery);