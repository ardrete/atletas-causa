


<?php if (isset($show_mini) && $show_mini == true) {
    include '_miniWidget.php';
} else { ?>
    <div class="ignitiondeck id-widget id-full" data-projectid="<?php echo stripslashes(get_the_title($the_deck->post_id)); ?>">
        <?php echo do_action('id_widget_before', $project_id, $the_deck); ?>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-12">
                    Fundacion que apoyas
                </label>

                <div class="col-md-12">
                    <h2 id="fundacion-resumen"> <?php echo get_post_meta($post_id, "ign_company_name", true) ?> </h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
                <?php if (!$custom || ($custom && isset($attrs['project_bar']))) { ?>
                    <div class="progress-wrapper">
                        <div class="progress-percentage"> <?php echo $the_deck->rating_per; ?>% </div>
                        <div class="progress-bar" style="width: <?php echo $the_deck->rating_per; ?>%">
                        </div>
                        <!-- end progress bar -->
                    </div>
                    <!-- end progress wrapper -->
                <?php } ?>
            <div class="form-group">
                <label class="control-label col-md-12">
                    Meta
                </label>

                <div class="col-md-12">
                    <h3>$<span
                            id="meta-resumen"><?php echo number_format(get_post_meta($post_id, "ign_fund_goal", true),2, '.',',') ?></span> <span
                            class="sup-value">MXN</span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-12">
                    Ha Conseguido
                </label>

                <div class="col-md-12">
                    <h3><span>
                            <?php echo $the_deck->p_current_sale ?>
                        </span><span class="sup-value">MXN</span></h3>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-12">
                    Donadores
                </label>

                <div class="col-md-12">
                    <h3><span><?php echo (isset($attrs['project_pledgers']) ? 0 : $the_deck->p_count->p_number);?></span></h3>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-12">
                    Quedan
                </label>
                <div class="col-md-12">
                    <h3>
                        <span id="dias-resumen">
                            <?php echo ($the_deck->days_left !== "" || $the_deck->days_left !== 0) ? $the_deck->days_left : '0';?>
                        </span>
                        <span class="sup-value">días</span>
                        <span id="hrs-resumen">
                            <?php echo ($the_deck->hours_left !== "" || $the_deck->hours_left <= 0 ) ? $the_deck->hours_left : '0';?>
                        </span>
                        <span class="sup-value">horas
                        </span>

                        <span id="min-resumen">
                            <?php echo ($the_deck->minutes_left !== "" || $the_deck->minutes_left <= 0) ? $the_deck->minutes_left : '0';?>
                        </span>
                        <span class="sup-value">min</span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md-12 ">
            <div class="btn-container">
                <?php if (isset($the_deck->end_type) && $the_deck->end_type == 'open' && is_id_licensed()) { ?>
                    <a href="<?php echo(isset($_GET['ig_embed_widget']) ? getProjectURLfromType($project_id) : getPurchaseURLfromType($project_id, 'purchaseform')); ?>"
                       class="btn btn-success btn-lg btn-block"><?php echo(isset($_GET['ig_embed_widget']) ? "Leer Mas" : "Apóyanos"); ?></a>
                <?php } else if (isset($the_deck->days_left) && $the_deck->days_left > 0 && is_id_licensed()) { ?>
                    <a href="<?php echo(isset($_GET['ig_embed_widget']) ? getProjectURLfromType($project_id) : getPurchaseURLfromType($project_id, 'purchaseform')); ?>"
                       class="btn btn-success btn-lg btn-block"><?php echo(isset($_GET['ig_embed_widget']) ? "Leer Mas" : "Ayudanos"); ?></a>
                <?php } ?>
            </div>

        </div>
    </div>
    <?php echo do_action('id_widget_after', $project_id, $the_deck); ?>
<?php } ?>
