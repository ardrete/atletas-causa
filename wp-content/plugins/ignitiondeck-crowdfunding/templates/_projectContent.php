<div class="id-content-wrap container <?php echo(isset($float) ? 'id-complete-projcont' : ''); ?>"
     data-projectid="<?php echo(isset($project_id) ? $project_id : ''); ?>">
    <div class="row product-post-output">
        <?php echo do_action('id_content_before', $project_id); ?>
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
            <h2 style="" class="block ">Resumen</h2>
        </div>
        <div class="col-md-4"></div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="resume-background-image"
                 style="background-image:url(<?php echo ID_Project::get_project_thumbnail($post_id); ?>)">

            </div>
            <div class="col-md-12 resumen-stats">
                <div class="col-md-3">
                    <i class="fa fa-heartbeat fa-2x"></i>

                    <h5>Actividad: <br><span
                            id="actividad-resumen"><?php echo get_user_meta($current_user->ID, "ign_user_activity", true) ?> </span>
                    </h5>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-tachometer fa-2x"></i>

                    <h5>KM:<br> <span id="distancia-resumen"><?php echo get_post_meta($post_id, "ign_project_distance", true) ?></span>
                    </h5>
                </div>
                <div class="col-md-3">
                    <i class="fa fa-usd  fa-2x"></i>

                    <h5>Valor por Metro: <br> $<span id="valor-km-resumen"><?php echo number_format(($the_deck->goal/($the_deck->km * 1000)), 2,'.', ','); ?></span></h5>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-map-marker fa-2x"></i>

                    <h5><span id="evento-resumen"><?php echo get_post_meta($post_id, "ign_project_event", true) ?></span></h5>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row resumen-sec1">
                <?php include ID_PATH . 'templates/_igWidget.php'; ?>

            </div>
        </div>
    </div>

    <div class="row moreInformation" >
        <div class="col-md-12">
            <ul id="ulSections" class="nav nav-pills nav-justified">
                <li role="presentation"><a aria-controls="home" role="tab" data-toggle="tab" href="#divDescription">Descripción</a></li>
                <li role="presentation"><a aria-controls="home" role="tab" data-toggle="tab" href="#divDonadores">Donadores</a></li>
                <li role="presentation"><a aria-controls="home" role="tab" data-toggle="tab" href="#divActualizaciones">Actualizaciones</a></li>
            </ul>
        </div>
        <div style="clear:both;"></div>
        <div class="tab-content">
            <div class="tab-pane fade" id="divDescription">
                
                <h4>Descripción</h4>
                <div class="long-description"><?php echo nl2br(html_entity_decode($project_long_desc)); ?></div>
                <div class="product-image-container">
                    <div>
                        <span class="image2"><?php echo((get_post_meta($post_id, "ign_product_image2", true) != "") ? '<img src="' . get_post_meta($post_id, "ign_product_image2", true) . '" />' : ''); ?></span>
                    </div>
                    <div>
                        <span class="image3"><?php echo((get_post_meta($post_id, "ign_product_image3", true) != "") ? '<img src="' . get_post_meta($post_id, "ign_product_image3", true) . '" />' : ''); ?></span>
                        <span class="image4"><?php echo((get_post_meta($post_id, "ign_product_image4", true) != "") ? '<img src="' . get_post_meta($post_id, "ign_product_image4", true) . '" />' : ''); ?></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="divDonadores">
                <?php echo do_action('id_content_after', $project_id); ?>
            </div>
            <div class="tab-pane fade" id="divActualizaciones">
                <?php
                $product_faq = apply_filters('idcf_faqs', get_post_meta($post_id, "ign_faqs", true));
                if ($product_faq && false) { ?>
                    <h3 class="product-dashed-heading"><?php echo $tr_Product_FAQ; ?></h3>
                    <div id="prodfaq">
                    <?php echo html_entity_decode(stripslashes($product_faq)); ?>
                    <div><?php do_action('id_faqs', $project_id); ?></div>
                    </div><?php
                } else if (has_action('id_faqs')) { ?>
                    <h3 class="product-dashed-heading"><?php echo $tr_Product_FAQ; ?></h3>
                    <div id="prodfaq">
                    <?php
                    do_action('id_faqs', $project_id); ?>
                    </div><?php } ?>

                <?php $product_updates = apply_filters('idcf_updates', get_post_meta($post_id, "ign_updates", true));
                if ($product_updates) { ?>
                    <h3 class="product-dashed-heading1"><?php echo $tr_Updates; ?></h3>
                    <div id="produpdates">
                    <?php echo html_entity_decode(stripslashes($product_updates)); ?>
                    <div><?php do_action('id_updates', $project_id); ?></div>
                    </div><?php
                } else if (has_action('id_updates')) { ?>
                    <h3 class="product-dashed-heading1"><?php echo $tr_Updates; ?></h3>
                    <div id="produpdates">
                    <?php
                    do_action('id_updates', $project_id); ?>
                    </div><?php
                } ?>
            </div>
        </div>
        <div class="clear"></div>

    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".ignition_product .alignnone").remove();

        jQuery("#ulSections a").click(function (e) {
            e.preventDefault();
            jQuery(this).tab('show');
        })

        jQuery("#ulSections a:first").click()


    });
</script>


