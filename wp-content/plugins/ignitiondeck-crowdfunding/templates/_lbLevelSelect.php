<?php
	// we need to hide/invalidate sold out levels
	if (isset($level)) {
		$level_invalid = getLevelLimitReached($project_id, $post_id, $level);
		if ($level_invalid) {
			$level = 0;
		}
	}
?>
<div class="ignitiondeck idc_lightbox mfp-hide">
	<div class="project_image" style="background-image: url(<?php echo $image; ?>);"><div class="aspect_ratio_maker"></div></div>
	<div class="lb_wrapper">
		<div class="form_header">
			<strong><?php _e('Paso 1:', 'ignitiondeck'); ?></strong> <?php _e('Especifique el número de metros con los que quiere contribuir', 'ignitiondeck'); ?> <em><?php echo get_the_title($post_id); ?></em>
		</div>
		<div class="form">
			<form action="<?php echo (isset($action) ? $action : ''); ?>" method="POST" name="idcf_level_select">
				<div class="form-row inline left twothird">
					<label for="level_select">Precio por metro</label>
					<input type="text" id="inpPrecioMetro" readonly="readonly" value="">
				</div>
				<div class="form-row inline left twothird">
					<label for="level_select"><?php _e('Cuanto quieres donar', 'ignitiondeck'); ?>
						<span style="display: none" class="idc-dropdown <?php echo ($the_deck->disable_levels == 'on' ? 'disable_levels' : ''); ?>">
							<select id="levelData" name="level_select" class="idc-dropdown__select level_select">
								<?php foreach ($level_data as $level) {
									if (empty($level->level_invalid) || !$level->level_invalid) {
										echo '<option value="'.$level->id.'" data-price="'.(isset($level->meta_price) ? $level->meta_price : '').'" data-desc="'.$level->meta_short_desc.'">'.$level->meta_title.'</option>';
									}
								}
								?>
							</select>
						</span>
					</label>
					<input type="text" class="total" id="total" name="total" value="0"/>
				</div>
				<div class="form-row inline third">
					<label for="total"><?php _e('Total de metros', 'ignitiondeck'); ?></label>
					<?php if (isset($pwyw) && $pwyw) { ?>
						<input type="text" name="metros" id="numMetros" readonly="readonly" value="<?php // echo total; ?>" />
					<?php }?>
				</div>
				<div class="form-row text">
					<p>
						<?php // echo description; ?>
					</p>
				</div>
				<div class="form-hidden">
					<input type="hidden" name="project_id" value="<?php echo $project_id; ?>"/>
				</div>
				<div class="form-row submit">
					<input type="submit" name="lb_level_submit" class="btn lb_level_submit" value="<?php _e('Siguiente Paso', 'ignitiondeck'); ?>"/>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function(){

			jQuery("#total").change(function(event){
				var priceMeter = parseFloat(jQuery("#valor-km-resumen").text());
				var priceTotal =  parseFloat(jQuery(this).val());

				var numMeters = priceTotal/priceMeter;
				var numMetersString = numMeters.toFixed(1);
				jQuery("#numMetros").val(numMetersString);
				var priceString = priceTotal.toFixed(2);
				jQuery("#levelData option:selected").data("price", priceString);
			});
			var priceMeters = parseFloat(jQuery("#valor-km-resumen").text());
			jQuery("#inpPrecioMetro").val(priceMeters.toFixed(2));
			jQuery("#total").change();
		});
	</script>
</div>