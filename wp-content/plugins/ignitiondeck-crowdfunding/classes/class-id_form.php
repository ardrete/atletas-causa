<?php

class ID_Form {

	var $fields;

	function __construct(
		$fields = null
		) 
	{
		$this->fields = $fields;
	}

	function build_form($vars = null, $postId = null) {
//		$output = '<ul>';
        global $current_user;
        get_currentuserinfo();
		$templateurl = get_template_directory_uri();

        $picture_user = get_user_meta($current_user->ID, 'ign_user_picture', true);
        $mensaje_user_picture = isset($picture_user) ?  "" : "Seleccione una imagen. <br>Click aqui.";

        $output = '<div class="container">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                	<div class="portlet box blue-dark" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-windows"></i> Crear Proyecto - <span class="step-title">
                                        Paso 1 de 4 </span>
                            </div>

                        </div>
                        <div class="portlet-body form">
                            <form action="#" class="form-horizontal" id="submit_form" method="POST">
                                <div class="form-wizard">
                                    <div class="form-body">
                                        <ul class="nav nav-pills nav-justified steps">
                                            <li>
                                                <a href="#tab1" data-toggle="tab" class="step">
                                                        <span class="number">
                                                        1 </span>
                                                        <span class="desc">
                                                        <i class="fa fa-check"></i> Configuración Perfil </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab2" data-toggle="tab" class="step">
                                                        <span class="number">
                                                        2 </span>
                                                        <span class="desc">
                                                        <i class="fa fa-check"></i> Configurar proyecto </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab3" data-toggle="tab" class="step active">
                                                        <span class="number">
                                                        3 </span>
                                                        <span class="desc">
                                                        <i class="fa fa-check"></i> Resumen </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div id="bar" class="progress progress-striped" role="progressbar">
                                            <div class="progress-bar progress-bar-success">
                                            </div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close" data-dismiss="alert"></button>
                                                Tienen algunos errores . Favor de verificar los campos indicados.
                                            </div>
                                            <div class="alert alert-success display-none">
                                                <button class="close" data-dismiss="alert"></button>
                                                Tu formulario fue exitoso!
                                            </div>


                                            <!--TAB 1 Start-->
                                            <div class="tab-pane active" id="tab1">
                                                <div class="row">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4 text-center">
                                                        <h3 style="" class="block ">Completa tu perfil</h3>
                                                    </div>

                                                    <div class="col-md-4"></div>

                                                    <div class="clearfix"></div>


                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4" id="photoCover">
                                                        <div class="profile-userpic photo-cover" style="background-image: url(' . $picture_user .'); background-repeat: no-repeat;" class="icon-square">
                                                            '.  $mensaje_user_picture .'
                                                        </div>
                                                        <canvas id="photoCanvas" style="display: none;"/>
                                                        <input type="file" style="display: none;" id="hdnPhotoFile" />
                                                    </div>
                                                    <div class="col-md-4"></div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Usuario
                                                            <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="usuario" type="text" class="form-control" name="usuario" value="' .$current_user->user_login . '" placeholder="Ingresa tu nombre de usuario"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Nombre
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="nombre" type="text" class="form-control" name="nombre" value="' .$current_user->user_firstname . '" placeholder="Ingresa tu nombre"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Apellidos
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="apellidos" type="text" class="form-control" name="apellidos" value="' .$current_user->user_lastname . '" placeholder="Ingresa tus apellidos"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Correo Electrónico
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="correoElectronico" type="text" class="form-control" name="correo" value="' .$current_user->user_email . '" placeholder="Ingresa tu correo"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">Teléfono
                                                                 <span class="required">*</span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="telefono" type="text" class="form-control" name="telefono" placeholder="Ingresa tu telefono" value="'. get_user_meta($current_user->ID, 'ign_user_phone', true) .'"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">¿De que estado eres?
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input type="hidden" style="display: none" id="currentState" value=" '. get_user_meta($current_user->ID, 'ign_user_state', true) .' "/>
                                                                <select name="estado" id="estados" class="form-control" placeholder="Ingresa tu estado de residencia">
                                                                    <option value=""></option>
                                                                    <option value="AS">Aguascalientes</option>
                                                                    <option value="BC">Baja California</option>
                                                                    <option value="BS">Baja California Sur</option>
                                                                    <option value="CC">Campeche</option>
                                                                    <option value="CS">Chiapas</option>
                                                                    <option value="CH">Chihuahua</option>
                                                                    <option value="CL">Coahuila</option>
                                                                    <option value="CM">Colima</option>
                                                                    <option value="DF">Distrito Federal</option>
                                                                    <option value="DG">Durango</option>
                                                                    <option value="GT">Guanajuato</option>
                                                                    <option value="GR">Guerrero</option>
                                                                    <option value="HG">Hidalgo</option>
                                                                    <option value="JC">Jalisco</option>
                                                                    <option value="MC">Estado de MMéxico</option>
                                                                    <option value="MN">Michoacán</option>
                                                                    <option value="MS">Morelos</option>
                                                                    <option value="NT">Nayarit</option>
                                                                    <option value="NL">Nuevo León</option>
                                                                    <option value="OC">Oaxaca</option>
                                                                    <option value="PL">Puebla</option>
                                                                    <option value="QT">Querétaro</option>
                                                                    <option value="QR">Quintana Roo</option>
                                                                    <option value="SP">San Luis Potosí</option>
                                                                    <option value="SL">Sinaloa</option>
                                                                    <option value="SR">Sonora</option>
                                                                    <option value="TC">Tabasco</option>
                                                                    <option value="TS">Tamaulipas</option>
                                                                    <option value="TL">Tlaxcala</option>
                                                                    <option value="VZ">Veracruz</option>
                                                                    <option value="YN">Yucatán</option>
                                                                    <option value="ZS">Zacatecas</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">¿Que deporte practicas?
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-7" id="deportes">
                                                                <span class="icon-square" data-value="Ciclismo">
                                                                    <img src="@templateUrl/images/cyclism.png" alt="Ciclismo"/>
                                                                </span>
                                                                <span class="icon-square" data-value="Corredor">
                                                                    <img src="@templateUrl/images/runnning.png" alt="Corredor"/>
                                                                </span>
                                                                <span class="icon-square" data-value="Natación">
                                                                       <img src="@templateUrl/images/natacion.png" alt="Natación"/>
                                                                </span>
                                                                <span class="icon-square" data-value="Triatlón">
                                                                    <img src="@templateUrl/images/triathlon.png" alt="Triatlón"/>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-offset-5 col-md-6">
                                                                <input readonly="readonly" id="deporte" type="text" class="form-control"
                                                                       name="deporte" value="'. get_user_meta($current_user->ID, 'ign_user_activity', true) .'"/>
                                                                <span class="help-block">Selecciona un deporte </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                ¿Por qué quieres ser un atleta
                                                                con causa?
                                                                <span class="required">*</span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <textarea id="razon" type="text" class="form-control" placeholder="Ingresa una breve descripcion"
                                                                          name="razon"> '. get_user_meta($current_user->ID, 'ign_user_reason', true) .' </textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--TAB 1 Ends-->

                                            <!--TAB 2 Start-->
                                            <div class="tab-pane" id="tab2">

                                                <div class="row">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4 text-center">
                                                        <h3 style="" class="block ">Configurar proyecto</h3>
                                                    </div>
                                                    <div class="col-md-4"></div>

                                                    <div class="clearfix"></div>

                                                    <div class="col-md-12 project-info">

                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="project-square techo-pais-project">
                                                                    <span class="poject-square-text">Un techo para mi país A.C.</span>
                                                                </div>
                                                                <span class="project-help-info fundacion-descr">Busca superar la pobreza de los asentamientos precarios en México,
                                                                promoviendo el desarrollo comunitario</span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="project-square canto-florece-project">
                                                                    <span class="poject-square-text">Canto que florece A.C.</span>
                                                                </div>
                                                                <span class="project-help-info  fundacion-descr">Construir atmósferas educativas que fomenten la cultura de la paz para integrar comunidades, a través de la música.
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="project-square nadie-rinde-project">
                                                                    <span class="poject-square-text">Aqui nadie se rinde A.C.</span>
                                                                </div>
                                                                <span class="project-help-info fundacion-descr">Se dedica a financiar transplantes de médula espinal para niñas y niños así com terapias con las familias en todo el proceso.
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-4">
                                                                <div class="project-square sport-talent-project">
                                                                    <span class="poject-square-text">Sport Talent</span>
                                                                </div>
                                                                <span class="project-help-info fundacion-descr">Otorga apoyoa a atletas de alto desempeño que luchan por destacar.
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="project-square fund-becar-project">
                                                                    <span class="poject-square-text">Fundación Becar IAP</span>
                                                                </div>
                                                                <span class="project-help-info fundacion-descr">Otorga apoyo a niños, niñas y jóvenes con devengada económia para que puedan recibir educación integral y de calidad.
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                Elige la fundación a la que quieres apoyar
                                                                <span class="required">*</span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <select name="fundacion" id="fundaciones" class="form-control" placeholder="Selecciona una fundacion">
                                                                    <option value=""></option>
                                                                    <option value="techo-para-mi-pais">Un Techo para mi pais
                                                                        A.C
                                                                    </option>
                                                                    <option value="canto-florece">Canto que florece A.C.
                                                                    </option>
                                                                    <option value="aqui-nadie-se-rinde">Aqui nadie se rinde
                                                                        A.C
                                                                    </option>
                                                                    <option value="sport-talent">Sport Talent
                                                                    </option>
                                                                    <option value="fundacion-becar">Fundación Becar</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                ¿En que competencia o evento participarás?
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="competencia" type="text" class="form-control" name="competencia" placeholder="Ingresa el nombre del evento"/>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                ¿Que distancia recorrerás (en Km)?
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <input id="distancia" type="text" class="form-control" name="distancia" placeholder="Ingresa la distancia"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">¿Cuando finaliza tu proyecto?
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="input-group input-medium date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">
                                                                    <input type="text" class="form-control" id="fechaFinaliza" name="fechaFinaliza" readonly placeholder="Selecciona una fecha"/>
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                ¿Cuanto quieres recaudar?
                                                                <span class="required">* </span>
                                                            </label>

                                                            <div class="col-md-7" id="cantidades">
                                                                <span class="label-square" data-value="1000">
                                                                    $1,000
                                                                </span>
                                                                <span class="label-square" data-value="2000">
                                                                    $2,000
                                                                </span>
                                                                <span class="label-square" data-value="5000">
                                                                    $5,000
                                                                </span>
                                                                <span class="label-square" data-value="10000">
                                                                    $10,000
                                                                </span>
                                                            </div>
                                                            <div class="col-md-offset-5 col-md-6">
                                                                <input type="text" class="form-control" placeholder="Otra Cantidad"
                                                                       name="cantidad" id="fondos"/>
                                                                <span class="help-block">Selecciona una cantidad </span>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                ¿Qué le dirias a tu gente para que te apoye?
                                                                <span class="required">*</span>
                                                            </label>

                                                            <div class="col-md-6">
                                                                <textarea id="descripcion" type="text" class="form-control"
                                                                          name="descripcion"> </textarea>
                                                            </div>
                                                        </div>



                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                Sube una imagen como fondo de tu proyecto
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-7" id="projectCover">
                                                                <div class="project-cover">
                                                                    Seleccione una imagen. 500 X 500 pixeles.<br> Click aqui
                                                                </div>
                                                                <canvas id="projectCanvas" style="display: none;"/>
                                                                <input type="file" style="display: none;" id="hdnProjectFile"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-5">
                                                                ¿Cuanto valen tus km.?
                                                                <span class="required">*</span>
                                                            </label>

                                                            <div class="col-md-7 calculo-km">
                                                                <div class="col-sm-11">
                                                                    <div class="col-md-8">Distancia (en km):</div>
                                                                    <div id="distanciaKm" class="col-md-4"></div>
                                                                    <div class="col-md-8">Distancia (en metros):</div>
                                                                    <div id="distanciaMts" class="col-md-4"></div>
                                                                    <div class="col-md-8">Fondos a recaudar:</div>
                                                                    <div id="fondosRecaudar" class="col-md-4"></div>
                                                                </div>
                                                                <div class="col-sm-11">
                                                                    <div class="col-md-8">Cada metro vale: </div>
                                                                    <div id="valorMts" class="col-md-4"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                            <!--TAB 2 Ends-->

                                            <!--TAB 3 Start-->
                                            <div class="tab-pane" id="tab3">
                                                <div class="row">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4 text-center">
                                                        <h3 style="" class="block ">Resumen</h3>
                                                    </div>
                                                    <div class="col-md-4"></div>

                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <canvas id="photoResumen" class="canvas-resumen"></canvas>
                                                        <div class="col-md-12 resumen-stats">
                                                            <div class="col-md-3">
                                                                <i class="fa fa-heartbeat fa-2x"></i>
                                                                <h5>Actividad: <br><span id="actividad-resumen"></span></h4>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <i class="fa fa-tachometer fa-2x"></i>
                                                                <h5>KM: <br><span id="distancia-resumen"></span></h4>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <i class="fa fa-usd  fa-2x"></i>
                                                                <h5>Valor por Metro: <br> <span id="valor-km-resumen"></span></h4>
                                                                <input type="hidden" id="hidValorMts"/>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <i class="fa fa-map-marker fa-2x"></i>
                                                                <h5>Evento: <br> <span id="evento-resumen"></span></h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row resumen-sec1">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-12">
                                                                        Fundación que apoyas
                                                                    </label>
                                                                    <div class="col-md-12">
                                                                        <h2 id="fundacion-resumen"></h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">

                                                                <div class="progress-wrapper">
                                                                    <div class="progress-percentage"> 0% </div>
                                                                    <div class="progress-bar" style="width: 0%">
                                                                    </div>
                                                                    <!-- end progress bar -->
                                                                </div>
                                                                <!-- end progress wrapper -->

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-12">
                                                                        Meta
                                                                    </label>
                                                                    <div class="col-md-12">
                                                                        <h3>$<span id="meta-resumen"> 0 </span><span class="sup-value">MXN</span></h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-12">
                                                                        Ha Conseguido
                                                                    </label>
                                                                    <div class="col-md-12">
                                                                        <h3>$<span >0</span><span class="sup-value">MXN</span></h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-12">
                                                                        Donadores
                                                                    </label>
                                                                    <div class="col-md-12">
                                                                        <h3><span>0</span></h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-12">
                                                                        Quedan
                                                                    </label>
                                                                    <div class="col-md-12">
                                                                        <h3>
                                                                            <span id="dias-resumen">0</span><span class="sup-value">días</span>
                                                                            <span id="hrs-resumen">0</span><span class="sup-value">horas</span>
                                                                            <span id="min-resumen">0</span><span class="sup-value">min</span>
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <!--TAB 3 Ends-->


                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-5 col-md-9">
                                                <a href="javascript:;" class="btn default button-previous">
                                                    <i class="m-icon-swapleft"></i> Regresar </a>
                                                <a href="javascript:;" class="btn blue button-next">
                                                    Continuar <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                                <a href="javascript:;" class="btn green button-submit">
                                                    Enviar <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-success">
                <div class="modal-dialog model-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">¡Estas a punto de convertirte en un Atleta con Causa!</h3>
                        </div>
                        <div class="modal-body">
                            <h5>En un plazo de 24 horas te llegara un correo de confirmación con tu proyecto dado de alta donde te proporcionaremos el link para que puedas compartir con todos esta gran causa</h5>
                            <div>
                                <img src="@templateUrl/images/logo-atletas.png"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <a class="btn btn-default" href="/como-recaudar/">Cómo Recaudar</a>
                                <a class="btn btn-default" href="/">Ir al Inicio</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- END PAGE CONTENT-->
            <script>
                jQuery(document).ready(function () {
                    Metronic.init();
                    FormWizard.init();
                });
            </script>
        </div>';

        $output = str_replace("@templateUrl", $templateurl, $output);



		return $output;
	}
}
?>
